import { connect } from 'react-redux'
import Clock from './clock'
import Counter from './counter'
import From from './from'
function Examples({ lastUpdate, light }) {
  return (
    <div>
      <Clock lastUpdate={lastUpdate} light={light} />
      <Counter />
      <From />
    </div>
  )
}

function mapStateToProps(state) {
  const { lastUpdate, light } = state
  return { lastUpdate, light }
}

export default connect(mapStateToProps)(Examples)
