import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../store/store'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import green from '@material-ui/core/colors/green';

const styles = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    }
}
class from extends Component {
    addTodo = (text) => {
        const { dispatch } = this.props
        dispatch(addTodo(text))
    }

    render() {
        let input
        let { todo, classes } = this.props
        console.log(classes)
        return (
            <div>
                <form
                    onSubmit={e => {
                        e.preventDefault()
                        if (!input.value.trim()) {
                            return
                        }
                        console.log(input.value)
                        this.addTodo(input.value)
                        input.value = ''
                    }}
                >
                    <input />
                    <FormControl color={green} className={classes.formControl}>
                        <InputLabel htmlFor="name-simple">Name</InputLabel>
                        <Input color="secondary" onChange={event => {
                            // e.preventDefault()
                            // if (!event.target.value.trim()) {
                            //     return
                            // }
                            console.log(event.target.value)
                            
                        }} id="name-simple" />
                    </FormControl>
                    <button onClick={()=>{this.addTodo(input)}}>
                        Add Todo
                     </button>
                </form>

                <ul>

                    {todo.map(v =>
                        (<li key={v}> {v}
                        </li>))}
                </ul>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { todo } = state
    return { todo }
}

export default connect(mapStateToProps)(withStyles(styles)(from))