import React from 'react'
import { connect } from 'react-redux'
import { startClock, serverRenderClock } from '../store/store'
import Examples from '../components/examples'
import Head from '../components/head'
import Nav from '../components/nav'
class Index extends React.Component {
  constructor(props){
    super(props)
    
  }
  static getInitialProps({ reduxStore, req }) {
    const isServer = !!req
    reduxStore.dispatch(serverRenderClock(isServer))
    console.log(isServer)
    return {}
  }

  componentDidMount() {
    const { dispatch } = this.props
    
    this.timer = startClock(dispatch)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  render() {
    return (
      <div>
      <Head title='Home'/>
      <Nav />
      <Examples />
      </div>
    )
  }
}


export default connect()(Index)
